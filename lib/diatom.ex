defmodule Diatom do
  @moduledoc """
  Diatom makes HTML from markdown.
  """

  @doc """
  Transform articles defined by markdown and output static HTML files to ./public/

  ## Examples

      iex> Diatom.run
      [:ok, :ok]

  """

  defmodule Article do
    defstruct slug: "", title: "", date: "", intro: "", body: "", photo: ""
  end

  @dist "./public/"
  @themed "./src/themes/active/"
  @mdbase "./src/content/"

  alias Elixir.Diatom.Article, as: Article
  alias Elixir.Diatom.Transform, as: Transform
  alias Elixir.Diatom.Themes, as: Themes

  def run do
    clear_destination()

    if not File.exists?(@themed) do
      run_vanilla()
    else
      run_theme()
    end
  end

  ####################
  ####################
  ####################

  def clear_destination do
    p = Path.expand(@dist)

    if File.exists?(p) do
      case File.dir?(p) do
        true -> File.rm_rf!(p)
        false -> File.rename(@dist, p <> "-old")
      end
    end

    File.mkdir!(p)
  end

  def copy_assets(from \\ "./src/assets") do
    if File.exists?(from) and File.dir?(from) do
      File.cp_r!(from, @dist <> "assets")
    end
  end

  defp run_vanilla do
    copy_assets()
    transform_home()
    transform_articles()
  end

  defp run_theme do
    copy_assets(@themed <> "assets")
    theme_home()
    transform_articles()
  end

  def transform_home do
    # todo move data to conf/md
    m = %{
      :theme => :vanilla,
      :head => [title: "Vanilla", description: "test", baseurl: "."],
      :header => [
        title: "Vanilla",
        description: "test",
        baseurl: ".",
        heroimg: "http://mrmrs.github.io/photos/display.jpg"
      ],
      :footer => [year: DateTime.utc_now().year],
      :previews => hydrate_previews()
    }

    html = Transform.home(m)

    @dist
    |> Path.join("index.html")
    |> File.write(html)
  end

  def theme_home do
    # todo move data to conf/md
    m = %{
      :theme => :tbd,
      :head => [title: "Themed", description: "theme test", baseurl: "."],
      :header => [title: "Themed", description: "theme test", baseurl: ".", heroimg: ""],
      :footer => [year: DateTime.utc_now().year],
      :previews => hydrate_previews()
    }

    html = Themes.home(m)

    @dist
    |> Path.join("index.html")
    |> File.write(html)
  end

  defp hydrate_previews, do: hydrate_a_prev() ++ hydrate_g_prev()

  defp hydrate_a_prev(from \\ "articles/**/*.md") do
    Path.wildcard(@mdbase <> from)
    |> Enum.map(fn x ->
      a = hydrate_article(x)
      [photo: a.photo, body: a.body, intro: a.intro, category: "articles"]
    end)
  end

  defp hydrate_g_prev(from \\ "gallery/**/*.md") do
    Path.wildcard(@mdbase <> from)
    |> Enum.reduce([], &gp_c/2)
  end

  defp gp_c(el, acc) do
    # todo overload hydrate_article as quick&dirty
    a = hydrate_article(el)
    n = Enum.count(acc)

    case n do
      0 ->
        [[photo: a.photo, body: a.body, title: a.title, category: "gallery", position: n] | []]

      _ ->
        [[photo: a.photo, body: a.body, title: a.title, category: "gallery", position: n] | acc]
    end
  end

  def transform_articles do
    f = @mdbase <> "articles/**/*.md"
    b = @mdbase <> "articles"
    d = @dist <> "articles"
    transform_content(f, b, d, :articles)
  end

  defp transform_content(
         from,
         base,
         dist,
         category
       ) do
    files = Path.wildcard(from)
    b = Path.expand(base)

    Enum.map(files, fn x ->
      a = Path.expand(Path.dirname(x))
      r = Path.relative_to(a, b)
      p = Path.join(dist, r)

      if not File.exists?(p) do
        File.mkdir_p!(p)
      end

      case category do
        :articles -> hydrate_article(x) |> write_html(p)
      end
    end)
  end

  defp hydrate_article(filepath) do
    a = %Article{
      slug: Path.basename(filepath, ".md")
    }

    filepath |> File.read!() |> split |> extract(a)
  end

  defp write_html(article, dist) do
    kv = [
      date: article.date,
      title: article.title,
      body: article.body
    ]

    html = article(kv)

    dist
    |> Path.join(article.slug <> ".html")
    |> File.write(html)
  end

  def split(data) do
    [frontmatter, markdown] = String.split(data, ~r/\n-{3,}\n/, parts: 2)
    {parse_yaml(frontmatter), Earmark.as_html(markdown)}
  end

  def parse_yaml(yaml) do
    [parsed] = :yamerl_constr.string(yaml)
    parsed
  end

  defp extract({props, htmlresult}, article) do
    %{
      article
      | title: get_prop(props, "title"),
        date: Timex.parse!(get_prop(props, "date"), "{ISOdate}"),
        intro: get_prop(props, "intro"),
        photo: get_prop(props, "photo"),
        body: get_body(htmlresult)
    }
  end

  defp get_prop(props, key) do
    case :proplists.get_value(String.to_charlist(key), props) do
      :undefined -> nil
      x -> to_string(x)
    end
  end

  defp get_body(data) do
    case data do
      {:ok, html, _} -> html
      {:error, _, _} -> ""
    end

    # todo capture err to log
  end

  def article(m) do
    if File.exists?(@themed) do
      Themes.article(m)
    else
      Transform.article(m)
    end
  end
end
