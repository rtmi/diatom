defmodule Diatom.Themes do
  @moduledoc """
  Themes are EEx templates for skinning configuration.
  """

  require EEx

  def previews(m) do
    Enum.map(m, fn x -> preview(x) end)
  end

  EEx.function_from_file(
    :def,
    :article,
    Path.expand("./src/themes/active/articles/article.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(
    :defp,
    :preview,
    Path.expand("./src/themes/active/articles/preview.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(:def, :home, Path.expand("./src/themes/active/index.html.eex"), [
    :assigns
  ])

  EEx.function_from_file(
    :def,
    :head,
    Path.expand("./src/themes/active/partials/head/head.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(
    :def,
    :header,
    Path.expand("./src/themes/active/partials/header/header.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(
    :def,
    :footer,
    Path.expand("./src/themes/active/partials/footer/footer.html.eex"),
    [
      :assigns
    ]
  )
end
