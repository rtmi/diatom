defmodule Diatom.Transform do
  @moduledoc """
  Transform are EEx templates for vanilla static configuration.
  """

  require EEx

  def previews(m) do
    Enum.map(m, fn x ->
      case x[:category] do
        "gallery" -> previewgallery(x)
        "articles" -> preview(x)
      end
    end)
  end

  EEx.function_from_file(:def, :article, Path.expand("./src/layouts/articles/article.html.eex"), [
    :assigns
  ])

  EEx.function_from_file(
    :defp,
    :preview,
    Path.expand("./src/layouts/articles/preview.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(:def, :home, Path.expand("./src/layouts/index.html.eex"), [:assigns])

  EEx.function_from_file(:def, :head, Path.expand("./src/layouts/partials/head/head.html.eex"), [
    :assigns
  ])

  EEx.function_from_file(
    :def,
    :header,
    Path.expand("./src/layouts/partials/header/header.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(
    :def,
    :footer,
    Path.expand("./src/layouts/partials/footer/footer.html.eex"),
    [
      :assigns
    ]
  )

  EEx.function_from_file(
    :defp,
    :previewgallery,
    Path.expand("./src/layouts/gallery/preview.html.eex"),
    [
      :assigns
    ]
  )
end
