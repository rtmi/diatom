# see https://medium.com/@pentacent/getting-started-with-elixir-docker-982e2a16213c

FROM bitwalker/alpine-elixir as build

COPY . .

RUN export MIX_ENV=prod; \
    rm -Rf _build; \
    mix deps.get; \
    mix compile; 


