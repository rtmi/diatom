---
title: First post
date: 2016-04-24
photo: http://mrmrs.github.io/photos/009.jpg
---

Before it burned to the ground, the structure filled a small beach
inlet below the Cliff House, also owned by Adolph Sutro at the time.
Shortly after closing, a fire in 1966 destroyed the building
while it was in the process of being demolished. All that remains
of the site are concrete walls, blocked off stairs and
passageways, and a tunnel with a deep crevice in the middle. The
cause of the fire was arson. Shortly afterwards, the developer
left San Francisco and claimed insurance money.
