---
title: Photo of the sea and sky on the horizon with the foundations of a demolished house.
date: 2016-05-25
photo: http://mrmrs.github.io/photos/011.jpg
---
![](http://mrmrs.github.io/photos/011.jpg)
