---
title: Photo of the sea with mist covering the rocky formations near the shore.
date: 2016-05-25
photo: http://mrmrs.github.io/photos/012.jpg
---
![](http://mrmrs.github.io/photos/012.jpg)
