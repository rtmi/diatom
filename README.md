# Diatom

Static site generator written in Elixir.

## Quickstart

1. 
2. 
3. 

```

```






## Credits

Douglas Matoso's
 [NanoGen](https://github.com/doug2k1/nanogen)
 [(LICENSE)](https://github.com/doug2k1/nanogen/blob/master/LICENSE)

Sebastian Seilund's
 [Static Blog](https://github.com/sebastianseilund/phoenix-static-blog-example)

Philipp's
 [Elixir Docker guide](https://medium.com/@pentacent/getting-started-with-elixir-docker-982e2a16213c)

